﻿using ApplicationSettings.ExportSettings;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CafedralDB.Forms.Settings
{
	public partial class ExportSemesterSettings : Form
	{
		public ExportSemesterSettings()
		{
			InitializeComponent();
			ApplicationSettings.ExportSettings.Semester.FromRegistry();
			textBoxFITStartRow.Text = Semester.FITStartRow.ToString();
			textBoxMSFStartRow.Text = Semester.MSFStartRow.ToString();
			textBoxIDPOStartRow.Text = Semester.IDPOStartRow.ToString();
			textBoxMAGStartRow.Text= Semester.MAGStartRow.ToString();
			textBoxIndexColumn.Text= Semester.IndexColumn.ToString();
			textBoxGroupColumn.Text= Semester.GroupColumn.ToString();
			textBoxCourceColumn.Text= Semester.CourceColumn.ToString();
			textBoxDisciplineColumn.Text= Semester.DisciplineColumn.ToString();
			textBoxDisciplineCostColumn.Text= Semester.DisciplineCostColumn.ToString();
			textBoxLecFactColumn.Text= Semester.LecFactColumn.ToString();
			textBoxStudentsColumn.Text= Semester.StudentsColumn.ToString();
		}

		private void buttonSave_Click(object sender, EventArgs e)
		{
			Semester.FITStartRow = Convert.ToInt32(textBoxFITStartRow.Text);
			Semester.MSFStartRow = Convert.ToInt32(textBoxMSFStartRow.Text);
			Semester.IDPOStartRow = Convert.ToInt32(textBoxIDPOStartRow.Text);
			Semester.MAGStartRow = Convert.ToInt32(textBoxMAGStartRow.Text);
			Semester.IndexColumn = Convert.ToInt32(textBoxIndexColumn.Text);
			Semester.GroupColumn = Convert.ToInt32(textBoxGroupColumn.Text);
			Semester.CourceColumn = Convert.ToInt32(textBoxCourceColumn.Text);
			Semester.DisciplineColumn = Convert.ToInt32(textBoxDisciplineColumn.Text);
			Semester.DisciplineCostColumn = Convert.ToInt32(textBoxDisciplineCostColumn.Text);
			Semester.LecFactColumn = Convert.ToInt32(textBoxLecFactColumn.Text);
			Semester.StudentsColumn = Convert.ToInt32(textBoxStudentsColumn.Text);

			MessageBox.Show("Сохранено");

			Semester.ToRegistry();
		}
	}
}
