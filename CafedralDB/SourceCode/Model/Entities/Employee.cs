﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Сотрудник
    /// </summary>
   public  class Employee
    {
		int _ID;
        string name;
        int _JobID;
        int _AcademicDegreeID;
        int _AcademicRankID;
        float rate;
        int contractNumber;
        DateTime birthDay;

		public Employee(int id)
		{
			ID = id;
		}

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        internal int WorkingPositionID
        {
            get
            {
                return _JobID;
            }

            set
            {
				_JobID = value;
            }
        }

        internal int AcademicDegreeID
        {
            get
            {
                return _AcademicDegreeID;
            }

            set
            {
				_AcademicDegreeID = value;
            }
        }

        internal int AcademicRankID
        {
            get
            {
                return _AcademicRankID;
            }

            set
            {
				_AcademicRankID = value;
            }
        }

        public float Rate
        {
            get
            {
                return rate;
            }

            set
            {
                rate = value;
            }
        }

        public int ContractNumber
        {
            get
            {
                return contractNumber;
            }

            set
            {
                contractNumber = value;
            }
        }

        public DateTime BirthDay
        {
            get
            {
                return birthDay;
            }

            set
            {
                birthDay = value;
            }
        }

		public int ID
		{
			get
			{
				return _ID;
			}

			set
			{
				_ID = value;
			}
		}
	}
}
