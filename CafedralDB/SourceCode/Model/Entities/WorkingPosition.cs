﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Рабочая должность
    /// </summary>
    public class WorkingPosition
    {
		int _ID;
        string name;

		public WorkingPosition(int id)
		{
			_ID = id;
		}

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

		public int ID
		{
			get
			{
				return _ID;
			}

			set
			{
				_ID = value;
			}
		}
	}
}
