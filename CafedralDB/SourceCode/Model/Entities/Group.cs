﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Группа
    /// </summary>
    public class Group
    {
		int _ID;
        string name;
        List<int> _StudentsIDs;
        int _FacultyID;
        int _StudyQualificationID;
		int _StudyFormID;
        int _SpecialityID;
        int _StudentCount;
        int entryYear;

		public Group(int id)
		{
			_ID = id;
		}

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        internal List<int> Students
        {
            get
            {
                return _StudentsIDs;
            }

            set
            {
				_StudentsIDs = value;
            }
        }

        internal int Faculty
        {
            get
            {
                return _FacultyID;
            }

            set
            {
				_FacultyID = value;
            }
        }

        internal int StudyQualification
        {
            get
            {
                return _StudyQualificationID;
            }

            set
            {
				_StudyQualificationID = value;
            }
        }

        internal int Speciality
        {
            get
            {
                return _SpecialityID;
            }

            set
            {
				_SpecialityID = value;
            }
        }

        public int StudentCount
		{
            get
            {
                return _StudentCount;
            }

            set
            {
                _StudentCount = value;
            }
        }

        internal int EntryYear
        {
            get
            {
                return entryYear;
            }

            set
            {
                entryYear = value;
            }
        }

		public int StudyFormID
		{
			get
			{
				return _StudyFormID;
			}

			set
			{
				_StudyFormID = value;
			}
		}
	}
}
