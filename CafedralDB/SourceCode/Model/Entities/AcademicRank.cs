﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Ученое звание
    /// </summary>
    public class AcademicRank
    {
		int _ID;
        string name;

		public AcademicRank(int id)
		{
			ID = id;
		}

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

		public int ID
		{
			get
			{
				return _ID;
			}

			set
			{
				_ID = value;
			}
		}
	}
}
