﻿using System.Collections.Generic;

namespace Model
{
    /// <summary>
    /// Дисциплина
    /// </summary>
    public class Discipline
    {
		int _ID;
        string name;
        List<DisciplineParameter> parameters;
        DisciplineTypeEnum type;
		int _TypeID;
		int _DepartmentID;
		int _LectureCount;
		int _PracticeCount;
		int _LabCount;
		bool _kz;
		bool _KR;
		bool _KP;
		bool _RGR;
		bool _Zach;
		bool _Ekz;
		bool _Kons;

		int _UchPr;
		int _prPr;
		int _predDipPr;
		bool _KonsZaoch;
		bool _GEK;
		bool _GAK;
		bool _GAKPred;
		bool _DPRuk;
		bool _DopuskVKR;
		bool _RetzVKR;
		bool _DPretz;
		bool _ASPRuk;
		bool _MAGRuk;
		bool _MAGRetz;
		bool _RukKaf;
		int _NIIR;

		bool _Special = false;

		int _semesterID;
		int _specialityID;

		public Discipline(int id)
		{
			ID = id;
		}

        public string Descr
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public List<DisciplineParameter> Parameters
        {
            get
            {
                return parameters;
            }

            set
            {
                parameters = value;
            }
        }

        internal DisciplineTypeEnum Type
        {
            get
            {
                return type;
            }

            set
            {
                type = value;
            }
        }

        internal int DepartmentID
        {
            get
            {
                return _DepartmentID;
            }

            set
            {
				_DepartmentID = value;
            }
        }

		public int TypeID
		{
			get
			{
				return _TypeID;
			}

			set
			{
				_TypeID = value;
			}
		}

		public int LectureCount
		{
			get
			{
				return _LectureCount;
			}

			set
			{
				_LectureCount = value;
			}
		}

		public int PracticeCount
		{
			get
			{
				return _PracticeCount;
			}

			set
			{
				_PracticeCount = value;
			}
		}

		public int LabCount
		{
			get
			{
				return _LabCount;
			}

			set
			{
				_LabCount = value;
			}
		}

		public bool KR
		{
			get
			{
				return _KR;
			}

			set
			{
				_KR = value;
			}
		}

		public bool KP
		{
			get
			{
				return _KP;
			}

			set
			{
				_KP = value;
			}
		}

		public bool RGR
		{
			get
			{
				return _RGR;
			}

			set
			{
				_RGR = value;
			}
		}

		public bool Zach
		{
			get
			{
				return _Zach;
			}

			set
			{
				_Zach = value;
			}
		}

		public bool Ekz
		{
			get
			{
				return _Ekz;
			}

			set
			{
				_Ekz = value;
			}
		}

		public bool Kons
		{
			get
			{
				return _Kons;
			}

			set
			{
				_Kons = value;
			}
		}

		public int ID
		{
			get
			{
				return _ID;
			}

			set
			{
				_ID = value;
			}
		}

		public bool Kz
		{
			get
			{
				return _kz;
			}

			set
			{
				_kz = value;
			}
		}

		public int SemesterID
		{
			get
			{
				return _semesterID;
			}

			set
			{
				_semesterID = value;
			}
		}

		public int SpecialityID
		{
			get
			{
				return _specialityID;
			}

			set
			{
				_specialityID = value;
			}
		}

		public int UchPr
		{
			get
			{
				return _UchPr;
			}

			set
			{
				_UchPr = value;
			}
		}

		public int PrPr
		{
			get
			{
				return _prPr;
			}

			set
			{
				_prPr = value;
			}
		}

		public int PredDipPr
		{
			get
			{
				return _predDipPr;
			}

			set
			{
				_predDipPr = value;
			}
		}

		public bool KonsZaoch
		{
			get
			{
				return _KonsZaoch;
			}

			set
			{
				_KonsZaoch = value;
			}
		}

		public bool GEK
		{
			get
			{
				return _GEK;
			}

			set
			{
				_GEK = value;
			}
		}

		public bool GAK
		{
			get
			{
				return _GAK;
			}

			set
			{
				_GAK = value;
			}
		}

		public bool GAKPred
		{
			get
			{
				return _GAKPred;
			}

			set
			{
				_GAKPred = value;
			}
		}

		public bool DPRuk
		{
			get
			{
				return _DPRuk;
			}

			set
			{
				_DPRuk = value;
			}
		}

		public bool DopuskVKR
		{
			get
			{
				return _DopuskVKR;
			}

			set
			{
				_DopuskVKR = value;
			}
		}

		public bool RetzVKR
		{
			get
			{
				return _RetzVKR;
			}

			set
			{
				_RetzVKR = value;
			}
		}

		public bool DPretz
		{
			get
			{
				return _DPretz;
			}

			set
			{
				_DPretz = value;
			}
		}

		public bool ASPRuk
		{
			get
			{
				return _ASPRuk;
			}

			set
			{
				_ASPRuk = value;
			}
		}

		public bool MAGRuk
		{
			get
			{
				return _MAGRuk;
			}

			set
			{
				_MAGRuk = value;
			}
		}

		public bool MAGRetz
		{
			get
			{
				return _MAGRetz;
			}

			set
			{
				_MAGRetz = value;
			}
		}

		public bool RukKaf
		{
			get
			{
				return _RukKaf;
			}

			set
			{
				_RukKaf = value;
			}
		}

		public int NIIR
		{
			get
			{
				return _NIIR;
			}

			set
			{
				_NIIR = value;
			}
		}

		public bool Special
		{
			get
			{
				return _Special;
			}

			set
			{
				_Special = value;
			}
		}

		public override string ToString()
		{
			string res = string.Format("ID:{0}\nDescr:{1}\nLect:{2}\nPract:{3}\nLab:{4}",
			ID,name,_LectureCount,_PracticeCount,_LabCount);
			return res;
		}
	}
}
