﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Специальность
    /// </summary>
    public class Speciality
    {
		int _ID;
        string name;
        int _FacultyID;

		public Speciality(int id)
		{
			_ID = id;
		}

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public int Faculty
        {
            get
            {
                return _FacultyID;
            }

            set
            {
                _FacultyID = value;
            }
        }
    }
}
