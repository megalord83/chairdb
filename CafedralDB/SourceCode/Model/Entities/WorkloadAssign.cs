﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
	public class WorkloadAssign
	{
		int _ID;
		int _EmployeeID;
		int _WorkloadID;
		int _StudentsCount;
		int _Weeks;

		public WorkloadAssign(int id)
		{
			_ID = id;
		}

		public int EmployeeID
		{
			get
			{
				return _EmployeeID;
			}

			set
			{
				_EmployeeID = value;
			}
		}

		public int WorkloadID
		{
			get
			{
				return _WorkloadID;
			}

			set
			{
				_WorkloadID = value;
			}
		}

		public int StudentsCount
		{
			get
			{
				return _StudentsCount;
			}

			set
			{
				_StudentsCount = value;
			}
		}

		public int Weeks
		{
			get
			{
				return _Weeks;
			}

			set
			{
				_Weeks = value;
			}
		}
	}
}
