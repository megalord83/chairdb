﻿namespace Model
{
    /// <summary>
    /// Параметр дисциплины, влияющий на часы
    /// </summary>
    public struct DisciplineParameter
    {
       private string name;
       private double cost;

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public double Cost
        {
            get
            {
                return cost;
            }

            set
            {
                cost = value;
            }
        }
    }
}