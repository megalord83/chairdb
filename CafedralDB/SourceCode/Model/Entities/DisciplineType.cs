﻿namespace Model
{
	/// <summary>
	/// Тип дисциплины
	/// </summary>
	enum DisciplineTypeEnum
	{

		Simple, //Обычная дисциплина 
		Special //Особая дисциплина с расчетом на человека(ГЭК,ГАК, Руководство и т.д.)
	}

	public class DisciplineType
	{
		int _ID;
		string name;

		public DisciplineType(int id)
		{
			ID = id;
		}

		public string Name
		{
			get
			{
				return name;
			}

			set
			{
				name = value;
			}
		}

		public int ID
		{
			get
			{
				return _ID;
			}

			set
			{
				_ID = value;
			}
		}
	}
}