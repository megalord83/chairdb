﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Учебный год
    /// </summary>
    public class StudyYear
    {
        int _ID;
        int year;

		public StudyYear(int id)
		{
			_ID = id;
		}

        public int ID
        {
            get
            {
                return _ID;
            }

            set
            {
                _ID = value;
            }
        }

        public int Year
        {
            get
            {
                return year;
            }

            set
            {
                year = value;
            }
        }

        public override string ToString()
        {
            return string.Format("{0}-{1}",year,year+1);
        }
    }
}
