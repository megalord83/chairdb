﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Нагрузка
    /// </summary>
    public class Workload
    {
		int _ID;
        int _DisciplineID;
        int _GroupID;
        int _SemesterID;
        int _YearID;

		public Workload()
		{
			_ID = -1;
		}

		public Workload(int id) 
		{
			_ID = id;
		}

        public int Discipline
        {
            get
            {
                return _DisciplineID;
            }

            set
            {
                _DisciplineID = value;
            }
        }

        public int Group
        {
            get
            {
                return _GroupID;
            }

            set
            {
                _GroupID = value;
            }
        }

        public int Semester
        {
            get
            {
                return _SemesterID;
            }

            set
            {
                _SemesterID = value;
            }
        }

        public int Year
        {
            get
            {
                return _YearID;
            }

            set
            {
                _YearID = value;
            }
        }

		public int ID
		{
			get
			{
				return _ID;
			}

			set
			{
				_ID = value;
			}
		}
	}
}
