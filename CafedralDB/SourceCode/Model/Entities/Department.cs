﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Кафедра университета
    /// </summary>
    public class Department
    {
		int _ID;
        int codeNumber;
        string description;

		public Department(int id)
		{
			ID = id;
		}

        public int CodeNumber
        {
            get
            {
                return codeNumber;
            }

            set
            {
                codeNumber = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }

            set
            {
                description = value;
            }
        }

		public int ID
		{
			get
			{
				return _ID;
			}

			set
			{
				_ID = value;
			}
		}
	}
}
