﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Ученая степень
    /// </summary>
    public class AcademicDegree
    {
		int _ID;
        string name;

		public AcademicDegree(int id)
		{
			ID = id;
		}

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

		public int ID
		{
			get
			{
				return _ID;
			}

			set
			{
				_ID = value;
			}
		}

		public override string ToString()
		{
			return String.Format("ID:{0},\nDescr:{1}",_ID,name);
		}
	}
}
