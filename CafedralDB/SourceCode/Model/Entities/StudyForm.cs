﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Форма обучения
    /// </summary>
    enum StudyFormEnum
    {
        FullTime, //Очное обучение
        CorrespondenceTime, //Заочное обучение
        PartTime //Очно-заочное обучение
    }

	/// <summary>
	/// Форма обучения
	/// </summary>
	public class StudyForm
	{
		int _ID;
		string _Name;
		string _NameRus;

		public StudyForm(int id)
		{
			_ID = id;
		}

		public int ID
		{
			get
			{
				return _ID;
			}

			set
			{
				_ID = value;
			}
		}

		public string Name
		{
			get
			{
				return _Name;
			}

			set
			{
				_Name = value;
			}
		}

		public string NameRus
		{
			get
			{
				return _NameRus;
			}

			set
			{
				_NameRus = value;
			}
		}
	}
}
