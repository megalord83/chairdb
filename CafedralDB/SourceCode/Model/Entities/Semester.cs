﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Семестр
    /// </summary>
   public class Semester
    {
        int _ID;
        string name;
        int weekCount;

		public Semester(int id)
		{
			_ID = id;
		}

        public int ID
        {
            get
            {
                return _ID;
            }

            set
            {
                _ID = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public int WeekCount
        {
            get
            {
                return weekCount;
            }

            set
            {
                weekCount = value;
            }
        }

        public override string ToString()
        {
            return string.Format("Semester - ID:{0}, Descr:{1}, Weeks:{2}", ID, Name, WeekCount);
        }
    }
}
