﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
	public static class Calculator
	{
		public static float GetWorkloadCost(int workloadID)
		{
			Workload workload = DataManager.SharedDataManager().GetWorkload(workloadID);
			Discipline discipline = DataManager.SharedDataManager().GetDiscipline(workload.Discipline);
			Group group = DataManager.SharedDataManager().GetGroup(workload.Group);
			Semester semester = DataManager.SharedDataManager().GetSemester(workload.Semester);

			float workloadCost = 0;

			workloadCost += ApplicationSettings.CalculationSettings.LectureCost * discipline.LectureCount * semester.WeekCount;
			workloadCost += ApplicationSettings.CalculationSettings.LabCost * discipline.LabCount * semester.WeekCount;
			workloadCost += ApplicationSettings.CalculationSettings.PracticeCost * discipline.PracticeCount * semester.WeekCount;

			//Расчет консультаций по дисциплине
			if(group.StudyFormID==1)
			{
				workloadCost += ApplicationSettings.CalculationSettings.KonsCost * discipline.LectureCount * semester.WeekCount;
			}
			else
			{
				workloadCost += ApplicationSettings.CalculationSettings.KonsCost*3 * discipline.LectureCount * semester.WeekCount;
			}

			if(discipline.KR)
			{
				workloadCost += ApplicationSettings.CalculationSettings.KRCost * group.StudentCount;
			}

			if (discipline.KP)
			{
				workloadCost += ApplicationSettings.CalculationSettings.KPCost * group.StudentCount;
			}

			if(discipline.Ekz)
			{
				if (group.StudyFormID == 1)
				{
					workloadCost += ApplicationSettings.CalculationSettings.EkzCost * group.StudentCount+2;
				}
				else
				{
					workloadCost += 0.4f * group.StudentCount+2;
				}
			}

			if(discipline.Zach)
			{
				workloadCost += ApplicationSettings.CalculationSettings.ZachCost * group.StudentCount;
			}

			workloadCost += ApplicationSettings.CalculationSettings.UchPr * discipline.UchPr;
			workloadCost += discipline.ASPRuk ? ApplicationSettings.CalculationSettings.AspRuk : 0;//GEK
			workloadCost += ApplicationSettings.CalculationSettings.PrPr * discipline.PrPr;
			workloadCost += discipline.GEK ? ApplicationSettings.CalculationSettings.GEK * group.StudentCount*6 : 0;//GEK
			workloadCost += discipline.GAK ? ApplicationSettings.CalculationSettings.GAK * group.StudentCount*6 : 0;//GAK
			workloadCost += discipline.GAKPred ? ApplicationSettings.CalculationSettings.GAKPred * group.StudentCount : 0;//GAKPred
			workloadCost += discipline.DPRuk ? ApplicationSettings.CalculationSettings.DPruk*group.StudentCount : 0;//DPruk
			workloadCost += discipline.RukKaf? ApplicationSettings.CalculationSettings.RukKaf: 0;
			workloadCost += ApplicationSettings.CalculationSettings.NIIR * discipline.NIIR * group.StudentCount;
			return workloadCost;
		}

		public static float GetEmployeeAllWorkloadsCost(int employeeID, int yearID)
		{
			List<Workload> workloads = DataManager.SharedDataManager().GetEmployeeWorkloads(employeeID, yearID);

			float result=0;

			foreach(Workload workload in workloads)
			{
				result += GetWorkloadCost(workload.ID);
			}

			return result;
		}
	}
}
