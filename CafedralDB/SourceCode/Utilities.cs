﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CafedralDB.SourceCode
{
    public static class Utilities
    {
        #region Excel exstension methods
        public static string GetCellText(this Microsoft.Office.Interop.Excel.Worksheet worksheet, int row, int column)
        {
            return ((Microsoft.Office.Interop.Excel.Range)worksheet.Cells[row, column]).Text;
        }
        #endregion

    }
}
