﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationSettings
{
	public static class ImportSettings
	{
		public static int StartReadingRow = 5;
		public static int GroupColumn = 1;
		public static int SemesterColumn = 2;
		public static int WeeksColumn = 3;
		public static int DisciplineNameColumn = 4;
		public static int LecturesColumn = 5;
		public static int PracticesColumn = 6;
		public static int LabsColumn = 7;
		public static int KzColumn = 8;
		public static int KrColumn = 9;
		public static int KpColumn = 10;
		public static int EkzColumn = 11;
		public static int ZachColumn = 12;
		public static int OtherColumn = 13;

		public static void ToRegistry()
		{
			RegistryKey saveKey = Registry.LocalMachine.CreateSubKey("software\\ChairDB\\Import");
			saveKey.SetValue("StartReadingRow", StartReadingRow);
			saveKey.SetValue("GroupColumn", GroupColumn);
			saveKey.SetValue("WeeksColumn", WeeksColumn);
			saveKey.SetValue("DisciplineNameColumn", DisciplineNameColumn);
			saveKey.SetValue("LecturesColumn", LecturesColumn);
			saveKey.SetValue("LabsColumn", LabsColumn);
			saveKey.SetValue("SemesterColumn", SemesterColumn);
			saveKey.SetValue("PracticesColumn", PracticesColumn);
			saveKey.SetValue("KzColumn", KzColumn);
			saveKey.SetValue("KrColumn", KrColumn);
			saveKey.SetValue("KpColumn", KpColumn);
			saveKey.SetValue("EkzColumn", EkzColumn);
			saveKey.SetValue("ZachColumn", ZachColumn);
			saveKey.SetValue("OtherColumn", OtherColumn);
			saveKey.Close();
		}

		public static void FromRegistry()
		{
			try
			{
				RegistryKey readKey = Registry.LocalMachine.OpenSubKey("software\\ChairDB\\Import");
				StartReadingRow = Convert.ToInt32(readKey.GetValue("StartReadingRow"));
				GroupColumn = Convert.ToInt32(readKey.GetValue("GroupColumn"));
				SemesterColumn = Convert.ToInt32(readKey.GetValue("SemesterColumn"));
				WeeksColumn = Convert.ToInt32(readKey.GetValue("WeeksColumn"));
				DisciplineNameColumn = Convert.ToInt32(readKey.GetValue("DisciplineNameColumn"));
				LecturesColumn = Convert.ToInt32(readKey.GetValue("LecturesColumn"));
				LabsColumn = Convert.ToInt32(readKey.GetValue("LabsColumn"));
				PracticesColumn = Convert.ToInt32(readKey.GetValue("PracticesColumn"));
				KzColumn = Convert.ToInt32(readKey.GetValue("KzColumn"));
				KrColumn = Convert.ToInt32(readKey.GetValue("KrColumn"));
				KpColumn = Convert.ToInt32(readKey.GetValue("KpColumn"));
				EkzColumn = Convert.ToInt32(readKey.GetValue("EkzColumn"));
				ZachColumn = Convert.ToInt32(readKey.GetValue("ZachColumn"));
				OtherColumn = Convert.ToInt32(readKey.GetValue("OtherColumn"));
				readKey.Close();
			}
			catch (Exception err)
			{ }
		}

	}

	public static class CalculationSettings
	{
		public static float LectureCost = 1;
		public static float LabCost = 1;
		public static float PracticeCost = 1;
		public static float KonsCost = 2;
		public static float EkzCost = 0.33f;
		public static float KRCost = 2;
		public static float KPCost = 3;
		public static float ZachCost = 0.25f;
		public static float RGR = 0.25f;
		public static float UchPr = 0.25f;
		public static float PrPr = 0.25f;
		public static float PreddipPr = 0.25f;
		public static float KzZaoch = 0.25f;
		public static float GEK = 0.25f;
		public static float GAK = 0.25f;
		public static float GAKPred = 0.25f;
		public static float DPruk = 0.25f;
		public static float DopuskVKR = 0.25f;
		public static float RetzVKR = 0.25f;
		public static float DPRetz = 0.25f;
		public static float MAGRuk = 0.25f;
		public static float MagRetz = 0.25f;
		public static float RukKaf = 0.25f;
		public static float NIIR = 0.25f;
		public static float NIIRRukMag = 0.25f;
		public static float ASPpractice = 0.25f;
		public static float NIIRRukAsp = 0.25f;
		public static float DopuskDissMag = 0.25f;
		public static float NormocontrolMag = 0.25f;
		public static float DopuskBak = 0.25f;
		public static float NormocontrolBak = 0.25f;
		public static float AspRuk = 50f;

		public static void ToRegistry()
		{
			try
			{
				RegistryKey saveKey = Registry.LocalMachine.CreateSubKey("software\\ChairDB\\CalculationSettings");
				saveKey.SetValue("LectureCost", LectureCost);
				saveKey.SetValue("LabCost", LabCost);
				saveKey.SetValue("PracticeCost", PracticeCost);
				saveKey.SetValue("KonsCost", KonsCost);
				saveKey.SetValue("EkzCost", EkzCost);
				saveKey.SetValue("KRCost", KRCost);
				saveKey.SetValue("KPCost", KPCost);
				saveKey.SetValue("ZachCost", ZachCost);
				saveKey.Close();
			}
			catch (Exception exc)
			{
			}
		}

		public static void FromRegistry()
		{
			try
			{
				//RegistryKey readKey = Registry.LocalMachine.OpenSubKey("software\\ChairDB\\CalculationSettings");
				//LectureCost = Convert.ToInt32(readKey.GetValue("LectureCost"));
				//LabCost = Convert.ToInt32(readKey.GetValue("LabCost"));
				//PracticeCost = Convert.ToInt32(readKey.GetValue("PracticeCost"));
				//KonsCost = Convert.ToInt32(readKey.GetValue("KonsCost"));
				//EkzCost = Convert.ToInt32(readKey.GetValue("EkzCost"));
				//KRCost = Convert.ToInt32(readKey.GetValue("KRCost"));
				//KPCost = Convert.ToInt32(readKey.GetValue("KPCost"));
				//ZachCost = Convert.ToInt32(readKey.GetValue("ZachCost"));
				//readKey.Close();

				bool res = false;
				Model.DataManager.SharedDataManager();
				var cn = new System.Data.OleDb.OleDbConnection(Model.DataManager.Connection.ConnectionString);
				var cmd = new System.Data.OleDb.OleDbCommand();
				cn.Open();
				cmd.Connection = cn;
				cmd.CommandType = System.Data.CommandType.Text;
				cmd.CommandText = "Select ParameterName, Value From Normas";
				System.Data.OleDb.OleDbDataReader reader = cmd.ExecuteReader();
				// while there is another record present
				res = reader.HasRows;
				Dictionary<string, float> parameters = new Dictionary<string, float>();
				if (res)
				{
					while (reader.Read())
					{
						string name = reader[0].ToString();
						float value = Convert.ToSingle(reader[1]);
						parameters.Add(name, value);
					}
				}

				LectureCost = parameters["Лекция"];
				LabCost = parameters["Практическая работа"];
				PracticeCost = parameters["Лабораторная работа"];
				KonsCost = parameters["Консультация"];
				EkzCost = parameters["Экзамен"];
				KRCost = parameters["КР"];
				KPCost = parameters["КП"];
				ZachCost = parameters["Зачет"];
				RGR = parameters["РГР"];
				UchPr = parameters["Учебная практика"];
				PrPr = parameters["Производственная практика"];
				PreddipPr = parameters["Преддипломная практика"];
				KzZaoch = parameters["Контрольные задания заочников"];
				GEK = parameters["ГЭК"];
				GAK = parameters["ГАК"];
				GAKPred = parameters["ГАК председатель"];
				DPruk = parameters["ДП руководство"];
				DopuskVKR = parameters["Допуск к ВКР"];
				RetzVKR = parameters["Рецензия ВКР"];
				DPRetz = parameters["ДП рецензии"];
				MAGRuk = parameters["Магистранты руководство"];
				MagRetz = parameters["Магистранты рецензирование"];
				RukKaf = parameters["Руководство кафедрой"];
				NIIR = parameters["Научная работа"];
				NIIRRukMag = parameters["Руководство НИИР магистра"];
				ASPpractice = parameters["Практика аспирантов"];
				NIIRRukAsp = parameters["Руководство НИИР аспирантов"];
				DopuskDissMag = parameters["Допуск к защите диссертации магистрантов"];
				NormocontrolMag = parameters["Нормоконтроль  диссертации магистрантов"];
				DopuskBak = parameters["Допуск к защите бакалавров"];
				NormocontrolBak = parameters["Нормоконтроль бакалавров"];

				cn.Close();
			}
			catch (Exception err)
			{
				System.Windows.Forms.MessageBox.Show("Не удалось загрузить настройки:\r\n" + err.Message + "\r\n" + err.StackTrace);
			}
		}
	}
	namespace ExportSettings
	{
		public static class Semester
		{
			public static int FITStartRow = 9;
			public static int MSFStartRow = 16;
			public static int IDPOStartRow = 24;
			public static int MAGStartRow = 32;
			public static int IndexColumn = 1;
			public static int GroupColumn = 2;
			public static int CourceColumn = 3;
			public static int DisciplineColumn = 4;
			public static int DisciplineCostColumn = 5;
			public static int LecFactColumn = 6;
			public static int StudentsColumn = 9;

			public static void ToRegistry()
			{
				try
				{
					RegistryKey saveKey = Registry.LocalMachine.CreateSubKey("software\\ChairDB\\Export\\Semester");
					saveKey.SetValue("FITStartRow", FITStartRow);
					saveKey.SetValue("MSFStartRow", MSFStartRow);
					saveKey.SetValue("IDPOStartRow", IDPOStartRow);
					saveKey.SetValue("MAGStartRow", MAGStartRow);
					saveKey.SetValue("IndexColumn", IndexColumn);
					saveKey.SetValue("GroupColumn", GroupColumn);
					saveKey.SetValue("CourceColumn", CourceColumn);
					saveKey.SetValue("DisciplineColumn", DisciplineColumn);
					saveKey.SetValue("DisciplineCostColumn", DisciplineCostColumn);
					saveKey.SetValue("LecFactColumn", LecFactColumn);
					saveKey.SetValue("StudentsColumn", StudentsColumn);
					saveKey.Close();
				}
				catch (Exception err)
				{ }

			}

			public static void FromRegistry()
			{
				try
				{
					RegistryKey readKey = Registry.LocalMachine.OpenSubKey("software\\ChairDB\\Export\\Semester");
					FITStartRow = Convert.ToInt32(readKey.GetValue("FITStartRow"));
					MSFStartRow = Convert.ToInt32(readKey.GetValue("MSFStartRow"));
					IDPOStartRow = Convert.ToInt32(readKey.GetValue("IDPOStartRow"));
					MAGStartRow = Convert.ToInt32(readKey.GetValue("MAGStartRow"));
					IndexColumn = Convert.ToInt32(readKey.GetValue("IndexColumn"));
					GroupColumn = Convert.ToInt32(readKey.GetValue("GroupColumn"));
					CourceColumn = Convert.ToInt32(readKey.GetValue("CourceColumn"));
					DisciplineColumn = Convert.ToInt32(readKey.GetValue("DisciplineColumn"));
					DisciplineCostColumn = Convert.ToInt32(readKey.GetValue("DisciplineCostColumn"));
					LecFactColumn = Convert.ToInt32(readKey.GetValue("LecFactColumn"));
					StudentsColumn = Convert.ToInt32(readKey.GetValue("StudentsColumn"));
					readKey.Close();
				}
				catch (Exception err)
				{ }
			}
		}

		public static class Workload
		{

		}

		public static class IndPlan
		{
			public static int DisciplinesStartRow = 5;
			public static int[] YearsDescriptionRowCell = { 26, 6 };
			public static int[] TeacherFIORowCell = { 29, 8 };
			public static int DisciplineNameColumn = 3;
			public static int DisciplineDescrColumn = 4;
			public static int StudentsCountColumn = 5;
			public static int DisciplineSettingsStartColumn = 9;

			public static void ToRegistry()
			{
				try
				{
					RegistryKey saveKey = Registry.LocalMachine.CreateSubKey("software\\ChairDB\\Export\\IndPlan");

					saveKey.Close();
				}
				catch (Exception err)
				{ }
			}

			public static void FromRegistry()
			{
				try
				{
					RegistryKey readKey = Registry.LocalMachine.OpenSubKey("software\\ChairDB\\Export\\IndPlan");

					readKey.Close();
				}
				catch (Exception err)
				{ }
			}
		}


	}


}
